﻿using Assets.Scripts.com;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class MyWindow : EditorWindow
{
    public int xSize = 3;
    public int ySize = 3;
    ItemSO so;
    public string tmp;
    private bool isGen;

    [MenuItem("Window/MyWindow")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(MyWindow));
    }

    private void Generation()
    {
        Rect rect = new Rect(0, 200, 25, 25);

        for (int i = 0; i < ySize; i++)
        {
            for (int j = 0; j < xSize; j++)
            {
                GUI.Button(rect, "blc");
                rect.x += 25;
            }
            rect.x = 0;
            rect.y += 25;
        }

    }
    private void OnGUI()
    {

        xSize = (int)(GUI.HorizontalSlider(new Rect(10, 10, 100, 20), xSize, 3, 9));
        GUI.Label(new Rect(140, 10, 20, 20), xSize.ToString());
        try
        {
            tmp = GUI.TextField(new Rect(10, 50, 40, 20), ySize.ToString(), 1);

            if (tmp == "")
            {
                ySize = 3;
            }
            else
            {
                ySize = int.Parse(tmp);
            }
        }
        catch (Exception ex)
        {
            GUI.TextField(new Rect(10, 50, 40, 20), ySize.ToString(), 1);
        }
        if (GUI.Button(new Rect(0, 100, 90, 30), "Save"))
        {
            so = Resources.Load<ItemSO>("ItemSO");
            so.SizeX = xSize;
            so.SizeY = ySize;
            EventManager._GENCLICK(new EventData(so));
        }
        Generation();
    }
}
