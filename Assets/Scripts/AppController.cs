﻿using Assets.Scripts.com;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppController : MonoBehaviour
{
    [SerializeField]
    private GameObject _itemPrefab;

    [SerializeField]
    private GameObject _horizontalParentPrefab;
    [SerializeField]
    private Transform _verticaParent;

    void Start()
    {
        EventManager._GENCLICK += GenClick;
    }

    private void GenClick(IData data)
    {
        ItemSO iso = (ItemSO)((EventData)data).data;

        for (int i = 0; i < _verticaParent.childCount; i++)
        {
            Destroy(_verticaParent.GetChild(i).gameObject);
        }

        for (int i = 0; i < iso.SizeY; i++)
        {
            Transform t = Instantiate(_horizontalParentPrefab, _verticaParent).GetComponent<Transform>();

            for (int j = 0; j < iso.SizeX; j++)
            {
                Instantiate(_itemPrefab, t);
            }
        }
    }
}
