﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "itemSO")]
public class ItemSO : ScriptableObject
{
    public int SizeX;
    public int SizeY;

}
